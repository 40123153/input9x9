
########################### 1. 導入所需模組
import cherrypy
import os

########################### 2. 設定近端與遠端目錄
# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"

########################### 3. 建立主物件
class HelloWorld(object):
    @cherrypy.expose
    def index2(self, input1=None, input2=None):
        return "Hello world!"+str(input1)
    @cherrypy.expose
    def inputform(self, input1=None, input2=None):
        return "input form"+str(input1)
    #index.exposed = True
    @cherrypy.expose
    # version 2 use table
    def index(self, var1=10, var2=10):
        # initialize outstring
        outstring = ""
        # initialize count
        count = 0
        for i in range(1, int(var1)):
            for j in range(1, int(var2)):
                count += 1
                if count%9 == 0:
                    outstring += str(i) + "x" + str(j) + "=" + str(i*j) + "<br />"
                else:
                    outstring += str(i) + "x" + str(j) + "=" + str(i*j) + "&nbsp;"*4
        return outstring
########################### 4. 安排啟動設定
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

########################### 5. 在近端或遠端啟動程式
# 利用 HelloWorld() class 產生案例物件
root = HelloWorld()
# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(root, config = application_conf)
else:
    # 近端執行啟動
    '''
    cherrypy.server.socket_port = 8083
    cherrypy.server.socket_host = '127.0.0.1'
    '''
    cherrypy.quickstart(root, config = application_conf)
